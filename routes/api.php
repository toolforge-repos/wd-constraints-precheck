<?php
declare(strict_types=1);

use Laravel\Lumen\Routing\Router;

/** @var Router $router */

$router->group([
    'prefix' => 'api',
], function (Router $router) {
    $router->get('/', 'Api\MainController@index');
    $router->get('constraints/{propertyId:P\d+}', 'Api\ConstraintsController@constraints');
    $router->post('check', 'Api\ConstraintsController@check');
});
