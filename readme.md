# Wikidata Constraints Preliminary Checker

https://tools.wmflabs.org/putnik/wd-constraints-precheck/

## License

The Wikidata Constraints Preliminary Checker is open-sourced software licensed under the [Apache License 2.0](https://opensource.org/licenses/Apache-2.0)
