<?php
declare(strict_types=1);

namespace App\Models;

use App\Models\Contracts\ValueInterface;

class Value implements ValueInterface
{
    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function toString(): string
    {
        $value = $this->data['value'];

        if (\is_array($value)) {
            return (string)$value['id'];
        }

        return (string)$this->data['value'];
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
