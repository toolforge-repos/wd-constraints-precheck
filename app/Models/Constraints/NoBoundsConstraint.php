<?php
declare(strict_types=1);

namespace App\Models\Constraints;

use App\Models\Contracts\StatementInterface;

class NoBoundsConstraint extends Constraint
{
    public function isSatisfiedBy(StatementInterface $statement): bool
    {
        return true;
    }
}
