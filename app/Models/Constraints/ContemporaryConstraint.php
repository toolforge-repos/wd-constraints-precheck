<?php
declare(strict_types=1);

namespace App\Models\Constraints;

use App\Models\Contracts\StatementInterface;

class ContemporaryConstraint extends Constraint
{
    public function isSatisfiedBy(StatementInterface $statement): bool
    {
        return true;
    }
}
