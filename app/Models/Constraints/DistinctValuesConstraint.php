<?php
declare(strict_types=1);

namespace App\Models\Constraints;

use App\Library\WikidataSparqlClient;
use App\Models\Contracts\StatementInterface;

class DistinctValuesConstraint extends Constraint
{
    /**
     * @param StatementInterface $statement
     * @return bool
     * @throws \RuntimeException
     */
    public function isSatisfiedBy(StatementInterface $statement): bool
    {
        $sparql = new WikidataSparqlClient();
        $value = $statement->getSnak()->getValue()->toString();
        $query = sprintf(
            'SELECT ?item { ?item wdt:%s "%s" . } LIMIT 1',
            $statement->getProperty()->getId(),
            $value
        );

        $rows = $sparql->query($query);

        if ($rows->count() === 0) {
            return true;
        }

        foreach ($rows as $row) {
            $this->addViolation([
                'value'          => $value,
                'conflicts_with' => substr($row['item'], strrpos($row['item'], '/') + 1),
            ]);
        }

        return false;
    }
}
