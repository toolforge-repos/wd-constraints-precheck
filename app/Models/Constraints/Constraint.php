<?php
declare(strict_types=1);

namespace App\Models\Constraints;

use App\Models\Contracts\ConstraintInterface;
use Illuminate\Support\Collection;

abstract class Constraint implements ConstraintInterface
{
    protected $qualifiers;
    protected $violations;

    public function __construct(?iterable $qualifiers = null)
    {
        $this->qualifiers = collect($qualifiers)->map('collect');
        $this->violations = collect();
    }

    protected function getConstraintCode(): string
    {
        try {
            $shortClassName = (new \ReflectionClass($this))->getShortName();
            return str_replace('_constraint', '', snake_case($shortClassName));
        } catch (\ReflectionException $e) {
            return 'unknown';
        }
    }

    protected function getQualifiers(): Collection
    {
        return $this->qualifiers;
    }

    protected function getQualifierValues(string $qualifierId): Collection
    {
        return $this->qualifiers->get($qualifierId);
    }

    protected function addViolation(array $data): void
    {
        $this->violations->push(\array_merge([
            'constraint' => $this->getConstraintCode(),
        ], $data));
    }

    public function getViolations(): Collection
    {
        return $this->violations;
    }
}
