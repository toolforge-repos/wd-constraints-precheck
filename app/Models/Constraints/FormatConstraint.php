<?php
declare(strict_types=1);

namespace App\Models\Constraints;

use App\Models\Contracts\PropertyInterface;
use App\Models\Contracts\StatementInterface;
use Illuminate\Support\Collection;

class FormatConstraint extends Constraint
{
    protected $regexp;

    protected function init(PropertyInterface $property): void
    {
        $statements = $property->getConstraintDataByCode('format');
        if ($statements->count() !== 0) {
            /** @var Collection $qualifiers */
            $qualifiers = $statements->first()->getQualifiers();
            if ($qualifiers->count() !== 0) {
                $this->regexp = $qualifiers->first()->getValue()->toString();
            }
        }
    }

    public function isSatisfiedBy(StatementInterface $statement): bool
    {
        $this->init($statement->getProperty());

        $value = $statement->getSnak()->getValue()->toString();
        $result = (bool)preg_match('/^' . $this->regexp . '$/', $value);
        if ($result === false) {
            $this->addViolation([
                'value' => $value,
                'regex' => $this->regexp,
            ]);
        }

        return $result;
    }
}
