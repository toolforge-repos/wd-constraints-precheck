<?php
declare(strict_types=1);

namespace App\Models;

use App\Models\Contracts\PropertyInterface;
use App\Models\Contracts\SnakInterface;
use App\Models\Contracts\ValueInterface;

class Snak implements SnakInterface
{
    protected $data;
    protected $property;
    protected $value;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getPropertyId(): string
    {
        return $this->data['property'];
    }

    public function getProperty(): PropertyInterface
    {
        if ($this->property === null) {
            $this->property = new Property($this->getPropertyId());
        }

        return $this->property;
    }

    public function getValue(): ValueInterface
    {
        if ($this->value === null) {
            $this->value = new Value($this->data['datavalue']);
        }

        return $this->value;
    }
}
