<?php
declare(strict_types=1);

namespace App\Models;

use App\Models\Contracts\ItemInterface;
use App\Models\Contracts\PropertyInterface;
use App\Models\Contracts\SnakInterface;
use App\Models\Contracts\StatementInterface;
use Illuminate\Support\Collection;

class Statement implements StatementInterface
{
    protected $data;
    protected $id;
    protected $item;
    protected $property;
    protected $snak;
    protected $qualifiers;

    /**
     * Statement constructor.
     * @param array $data
     * @throws \RuntimeException
     */
    public function __construct(array $data)
    {
        $this->data = $data;

        $this->id = $data['id'];

        $itemId = preg_replace('/\$.*$/', '', $data['id']);
        $this->item = new Item($itemId);

        if (!isset($data['mainsnak']['property'])) {
            throw new \RuntimeException();
        }
    }

    public function getItem(): ItemInterface
    {
        return $this->item;
    }

    public function getPropertyId(): string
    {
        return $this->data['mainsnak']['property'];
    }

    public function getProperty(): PropertyInterface
    {
        if ($this->property === null) {
            $this->property = new Property($this->getPropertyId());
        }

        return $this->property;
    }

    public function getSnak(): SnakInterface
    {
        if ($this->snak === null) {
            $this->snak = new Snak($this->data['mainsnak']);
        }

        return $this->snak;
    }

    public function getQualifiers(): Collection
    {
        if ($this->qualifiers === null) {
            $this->qualifiers = collect($this->data['qualifiers'])->flatten(1)->map(function ($item) {
                return new Snak($item);
            });
        }

        return $this->qualifiers;
    }
}
