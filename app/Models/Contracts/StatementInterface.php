<?php
declare(strict_types=1);

namespace App\Models\Contracts;

use Illuminate\Support\Collection;

interface StatementInterface
{
    public function getItem(): ItemInterface;

    public function getPropertyId(): string;

    public function getProperty(): PropertyInterface;

    public function getSnak(): SnakInterface;

    public function getQualifiers(): Collection;
}
