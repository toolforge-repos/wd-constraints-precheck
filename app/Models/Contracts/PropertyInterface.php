<?php
declare(strict_types=1);

namespace App\Models\Contracts;

use Illuminate\Support\Collection;

interface PropertyInterface extends ItemInterface
{
    public function getConstraintIds(): Collection;

    public function getConstraintDataByCode(string $code): Collection;

    public function hasConstraintId(string $constraintId): bool;
}
