<?php
declare(strict_types=1);

namespace App\Models\Contracts;

interface ValueInterface
{
    public function toString(): string;
}
