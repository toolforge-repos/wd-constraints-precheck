<?php
declare(strict_types=1);

namespace App\Models\Contracts;

use Illuminate\Support\Collection;

interface ConstraintInterface
{
    public const CONSTRAINT_IDS = [
        'allowed_qualifiers'       => 'Q21510851',
        'allowed_units'            => 'Q21514353',
        'commons_link'             => 'Q21510852',
        'conflicts_with'           => 'Q21502838',
        'contemporary'             => 'Q25796498',
        'difference_within_range'  => 'Q21510854',
        'distinct_values'          => 'Q21502410',
        'format'                   => 'Q21502404',
        'inverse'                  => 'Q21510855',
        'item_requires_statement'  => 'Q21503247',
        'mandatory_qualifier'      => 'Q21510856',
        'multi_value'              => 'Q21510857',
        'no_bounds'                => 'Q51723761',
        'one_of'                   => 'Q21510859',
        'range'                    => 'Q21510860',
        'single_value'             => 'Q19474404',
        'symmetric'                => 'Q21510862',
        'type'                     => 'Q21503250',
        'used_as_qualifier'        => 'Q21510863',
        'used_as_reference'        => 'Q21528959',
        'used_for_values_only'     => 'Q21528958',
        'value_requires_statement' => 'Q21510864',
        'value_type'               => 'Q21510865',
    ];

    public function isSatisfiedBy(StatementInterface $statement): bool;

    public function getViolations(): Collection;
}
