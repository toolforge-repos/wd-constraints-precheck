<?php
declare(strict_types=1);

namespace App\Models\Contracts;

interface ItemInterface
{
    public function getId(): string;
}
