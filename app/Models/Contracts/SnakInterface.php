<?php
declare(strict_types=1);

namespace App\Models\Contracts;

interface SnakInterface
{
    public function getPropertyId(): string;

    public function getProperty(): PropertyInterface;

    public function getValue(): ValueInterface;
}
