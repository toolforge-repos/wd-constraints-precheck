<?php
declare(strict_types=1);

namespace App\Models;

use App\Library\WikidataClient;
use App\Models\Contracts\ConstraintInterface;
use App\Models\Contracts\PropertyInterface;
use App\Models\Contracts\StatementInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class Property extends Item implements PropertyInterface
{
    protected $constraintStatements;

    protected function getConstraintStatements(): Collection
    {
        if ($this->constraintStatements === null) {
            $itemId = $this->getId();
            $constraints = Cache::get('constraints_' . $itemId, function () use ($itemId) {
                /** @var WikidataClient $wikidata */
                $wikidata = app(WikidataClient::class);

                return $wikidata->getItemStatements($itemId, ['P2302']);
            });

            $this->constraintStatements = collect($constraints);
        }

        return $this->constraintStatements;
    }

    public function getConstraintIds(): Collection
    {
        return $this->getConstraintStatements()->map(function (StatementInterface $statement) {
            return $statement->getSnak()->getValue()->toString();
        });
    }

    public function hasConstraintId(string $constraintId): bool
    {
        return $this->getConstraintIds()->search($constraintId) !== false;
    }

    public function hasConstraintCode(string $code): bool
    {
        if (!\array_key_exists($code, ConstraintInterface::CONSTRAINT_IDS)) {
            return false;
        }

        return $this->hasConstraintId(ConstraintInterface::CONSTRAINT_IDS[$code]);
    }

    public function getConstraintDataByCode(string $code): Collection
    {
        if (!\array_key_exists($code, ConstraintInterface::CONSTRAINT_IDS)) {
            return collect();
        }
        $constraintId = ConstraintInterface::CONSTRAINT_IDS[$code];

        $constraints = $this->getConstraintStatements()->filter(
            function (StatementInterface $statement) use ($constraintId) {
                return $statement->getSnak()->getValue()->toString() === $constraintId;
            }
        );

        return $constraints;
    }
}
