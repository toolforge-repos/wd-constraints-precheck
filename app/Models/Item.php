<?php
declare(strict_types=1);

namespace App\Models;

use App\Models\Contracts\ItemInterface;
use Illuminate\Database\Eloquent\Model;

class Item implements ItemInterface
{
    protected $id;

    public function __construct(string $itemId)
    {
        $this->id = $itemId;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
