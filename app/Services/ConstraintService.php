<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Constraints\DistinctValuesConstraint;
use App\Models\Contracts\ConstraintInterface;
use App\Models\Contracts\StatementInterface;
use Illuminate\Support\Collection;

class ConstraintService
{
    protected $constraints = [];

    public function __construct()
    {
        foreach (ConstraintInterface::CONSTRAINT_IDS as $constraintCode => $constraintId) {
            $constraintClass = 'App\\Models\\Constraints\\' . studly_case($constraintCode) . 'Constraint';
            $this->constraints[$constraintId] = $constraintClass;
        }
    }

    public function getConstraintById(string $constraintId): ?ConstraintInterface
    {
        if (!\in_array($constraintId, $this->constraints)) {
            return null;
        }

        return new $this->constraints[$constraintId];
    }

    public function getStatementViolations(StatementInterface $statement): ?Collection
    {
        $property = $statement->getProperty();

        foreach ($this->constraints as $constraintId => $constraintClass) {
            if (!$property->hasConstraintId($constraintId)) {
                continue;
            }

            /** @var ConstraintInterface $constraint */
            $constraint = new $constraintClass();
            if (!$constraint->isSatisfiedBy($statement)) {
                return $constraint->getViolations();
            }
        }

        return null;
    }
}
