<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class MainController extends Controller
{
    public function index(): Response
    {
        $parsedown = new \Parsedown();

        $md = file_get_contents(base_path('readme.md'));
        $html = $parsedown->text($md);

        return response($html);
    }
}
