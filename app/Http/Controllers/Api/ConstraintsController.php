<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Statement;
use App\Services\ConstraintService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ConstraintsController extends Controller
{
    protected $constraintService;

    public function __construct(ConstraintService $constraintService)
    {
        $this->constraintService = $constraintService;
    }

    public function constraints(Request $request, string $propertyId): JsonResponse
    {
        return response()->json([
            'success'     => true,
            'property'    => $propertyId,
            'constraints' => [],
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \RuntimeException
     */
    public function check(Request $request): JsonResponse
    {
        $statement = new Statement($request->json()->all());
        $violations = $this->constraintService->getStatementViolations($statement);

        if ($violations === null) {
            return response()->json([
                'success' => true,
            ]);
        }

        return response()->json([
            'success'    => false,
            'violations' => $violations,
        ]);
    }
}
