<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class MainController extends Controller
{
    public function index(): JsonResponse
    {
        return response()->json([
            'info' => 'https://toolsadmin.wikimedia.org/tools/id/wd-constraints-precheck',
        ]);
    }
}
