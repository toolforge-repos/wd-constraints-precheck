<?php
declare(strict_types=1);

namespace App\Library;

use App\Models\Statement;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class WikidataClient
{
    protected $factory;

    protected function getItem(string $itemId): ?array
    {
        try {
            $client = new Client();
            $response = $client->get(config('wikibase.url'), [
                'query' => [
                    'action'    => 'wbgetentities',
                    'format'    => 'json',
                    'languages' => 'en',
                    'props'     => 'labels|descriptions|claims',
                    'ids'       => $itemId,
                ],
            ]);

            return \json_decode($response->getBody()->getContents(), true)['entities'][$itemId];
        } catch (\Throwable $exception) {
            return null;
        }
    }

    public function getItemStatements(string $id, ?array $propertyIds = null): Collection
    {
        $item = $this->getItem($id);
        if ($item === null) {
            return collect();
        }

        $statements = collect($item['claims'])->filter(function ($item, $key) use ($propertyIds) {
            return $propertyIds === null || \in_array($key, $propertyIds, true);
        });

        $statements = $statements->flatten(1)->map(function ($item) {
            return new Statement($item);
        });

        return $statements;
    }
}
