<?php
declare(strict_types=1);

namespace App\Library;

use BorderCloud\SPARQL\SparqlClient;
use Illuminate\Support\Collection;

class WikidataSparqlClient
{
    protected $client;

    public function __construct()
    {
        $this->client = new SparqlClient();
        $this->client->setEndpointRead(config('sparql.endpoint'));
    }

    /**
     * @param string $query
     * @return Collection
     * @throws \RuntimeException
     */
    public function query(string $query): Collection
    {
        $rows = $this->client->query($query, 'rows');
        $errors = $this->client->getErrors();
        if ($errors) {
            throw new \RuntimeException(print_r($errors, true));
        }

        return collect($rows['result']['rows']);
    }
}
