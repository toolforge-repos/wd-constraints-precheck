<?php
declare(strict_types=1);

namespace App\Providers;

use App\Library\WikidataClient;
use App\Library\WikidataSparqlClient;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(WikidataClient::class, WikidataClient::class);
        $this->app->singleton(WikidataSparqlClient::class, WikidataSparqlClient::class);
    }
}
