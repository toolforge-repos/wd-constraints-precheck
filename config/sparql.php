<?php
declare(strict_types=1);

return [
    'endpoint' => env('SPARQL_ENDPOINT', 'https://query.wikidata.org/sparql'),
];
