<?php
declare(strict_types=1);

return [
    'url'      => env('WIKIBASE_URL', 'https://www.wikidata.org/w/api.php'),
    'username' => env('WIKIBASE_USERNAME'),
    'password' => env('WIKIBASE_PASSWORD'),
];
